@echo off
rmdir build /s /q

mkdir  build\bin-lib-v1.0
mkdir  build\bin-lib-v2.0

mkdir  build\bin-program1
mkdir  build\bin-program2

mkdir  build\temp1
mkdir  build\temp2
mkdir  build\temp3


echo compile lib1:
csc /target:library /out:build\bin-lib-v1.0\lib.dll src-lib-v1.0\lib\%1\foo.cs
echo compile lib2:
csc /target:library /out:build\bin-lib-v2.0\lib.dll src-lib-v2.0\lib\%1\foo.cs

echo compile program with lib1:
csc /r:build\bin-lib-v1.0\lib.dll /out:build\bin-program1\main.exe src-program\%1\main.cs
echo compile programm with lib2:
csc /r:build\bin-lib-v2.0\lib.dll /out:build\bin-program2\main.exe src-program\%1\main.cs

echo run program compiled with lib 1.0 with lib 1.0:
copy build\bin-lib-v1.0\lib.dll build\temp1
copy build\bin-program1\main.exe build\temp1
build\temp1\main.exe

echo run program compiled with lib 1.0 with lib 2.0:
copy build\bin-lib-v2.0\lib.dll build\temp2
copy build\bin-program1\main.exe build\temp2
build\temp2\main.exe

echo run program compiled with lib 2.0 with lib 2.0:
copy build\bin-lib-v2.0\lib.dll build\temp2
copy build\bin-program2\main.exe build\temp2
build\temp2\main.exe