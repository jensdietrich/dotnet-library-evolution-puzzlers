using System;
public class Foo
{
	private int foo = 1;
	public class Inner
	{
		Foo f;
		public Inner(Foo f)
		{
			this.f=f;
		}
		public override String ToString()
		{
			return "Inner[foo="+f.foo+"]" ;
		}
	}
}