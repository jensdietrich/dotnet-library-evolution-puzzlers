using System;
namespace GeneraliseParamType2
{
	public class Foo 
	{
		public static void doIt(Class1 c)
		{
			Console.WriteLine( "C1" );
		}

		public static void doIt(Iinterface2 c)
		{
			Console.WriteLine( "I2" );
		}
	}
}