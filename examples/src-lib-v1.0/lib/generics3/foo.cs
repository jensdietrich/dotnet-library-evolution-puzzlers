using System;
using System.Runtime.Serialization;
public class Foo<T> where T: ISerializable , IComparable, new()
{
	public void foo(T t)
	{
		t.CompareTo("");
		Console.WriteLine(t);
	}	
}