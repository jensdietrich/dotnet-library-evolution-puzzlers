using System;
public class Foo
{
	int foo = 2;
	public class Inner
	{
		Foo f;
		public Inner(Foo f)
		{
			this.f=f;
		}
		public override String ToString()
		{
			return "Inner[foo="+f.foo+"]" ;
		}
	}
}