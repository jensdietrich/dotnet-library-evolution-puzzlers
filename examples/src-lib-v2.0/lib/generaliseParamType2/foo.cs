using System;
namespace GeneraliseParamType2
{
	public class Foo 
	{
		static void doIt(Iinterface1 c)
		{
			Console.WriteLine( "I1" );
		}

		static void doIt(Iinterface2 c)
		{
			Console.WriteLine( "I2" );
		}
	}
}